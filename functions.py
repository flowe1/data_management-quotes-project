# Functions for webapp

def ceil(n):
	if (n == int(n)):
		return n
	else:
		return int(n) + 1

def num_pages(numQuotes, perPage):
	return ceil(numQuotes / perPage)